# Books, Heart and Brain

Questo è il codice sorgente del [sito del gruppo di lettura](https://booksheartbrain.netlify.app/) Books, heart and brain.
Se non diversamente specificato dai componenti utilizzati nel sito, il codice è rilasciato con licenza CC0 1.0.

[![Logo](assets/booksheartandbrain.webp)](assets/booksheartandbrain.webp)


# Roadmap

Ecco le modifiche che pensiamo di fare a breve nel sito:

- Aggiunta di nuovi consigli;

[Crea un'issue](https://gitlab.com/booksheartbrain/booksheartbrain/-/issues/new) se trovi degli errori nel sito o se vorresti consigliarci qualche modifica al sito.

Modifiche già effettuate:

- Miglioramenti nell'accessibilità;