var header = "";
var footer = "";

//HEADER
//Header normale
header = `<header class="py-3">
            <nav class="navbar" role="navigation" aria-label="main navigation">
              <div class="navbar-brand">
                <a href="/index.html">
                  <img alt="Logo del gruppo" src="/assets/booksheartandbrain.webp" width="100" height="100">
                </a>
                <a role="button" class="navbar-burger" data-target="navMenu" aria-label="menu" aria-expanded="false">
                  <span aria-hidden="true"></span> 
                  <span aria-hidden="true"></span> 
                  <span aria-hidden="true"></span>
                </a>
              </div>
              <div class="navbar-menu" id="navMenu">
                <div class="navbar-end">
                  <a href="/index.html" class="navbar-item">
                    <i class="fa fa-home" aria-hidden="true"></i>&nbsp;Home
                  </a>
                  <a href="/pages/partecipa.html" class="navbar-item">
                    <i class="fa fa-users" aria-hidden="true"></i>&nbsp;Partecipa
                  </a>
                  <a href="/pages/paperbrains.html" class="navbar-item">
                    <i class="fa fa-microphone" aria-hidden="true"></i>&nbsp;Paper Brains
                  </a>
                  <a href="/pages/eventi/eventi.html" class="navbar-item">
                    <i class="fa fa-calendar" aria-hidden="true"></i>&nbsp;Eventi
                  </a>
                  <a href="/pages/ciaksilegge.html" class="navbar-item">
                    <i class="fa fa-film" aria-hidden="true"></i>&nbsp;Ciak si legge
                  </a>
                  <div class="navbar-item has-dropdown is-hoverable" tabindex="0">
                    <a class="navbar-link">
                      <i class="fa fa-book" aria-hidden="true"></i>&nbsp;Libri
                    </a>
                    <div class="navbar-dropdown is-right">
                      <a class="navbar-item" href="/pages/libri/letti.html" tabindex="0">
                        <i class="fa fa-bookmark" aria-hidden="true"></i>&nbsp;Il quaderno dei suggerimenti
                      </a>
                      <a class="navbar-item" href="/pages/libri/consigli.html" tabindex="0">
                        <i class="fa fa-certificate" aria-hidden="true"></i>&nbsp;Libri con bollino
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </nav>
          </header>
`;

//FOOTER
//Footer normale
footer += `<div class="content has-text-centered"><img src="/assets/undraw_book_lover_mkck.svg" alt="" class="vectorial-image is-centered">`;
//Linea webmaster
footer += `<p>Sito creato da <a href="https://gicorada.com/" target="_blank" rel="noopener noreferrer">Giacomo R.</a></p>`;
//Linea pagine sito
footer += `<p><a href="/pages/credits.html">Crediti</a> - <a href="/pages/privacy.html">Privacy</a></p>`;
//Pagina instagram
footer += `<p class="is-size-3"><a href="https://www.instagram.com/booksheartbrain/" target="_blank" rel="noopener noreferrer" title="Pagina instagram"><i class="fa fa-instagram hvr-grow" aria-hidden="true" title="Instagram"></i></a> &nbsp;`;
//Canale youtube
footer += `<a href="https://www.youtube.com/channel/UCDhYFbVeJsZxkshbSm5lvDA" target="_blank" rel="noopener noreferrer" title="Canale Youtube"><i class="fa fa-youtube-play hvr-grow" aria-hidden="true" title="Youtube"></i></a> &nbsp;`;
//Pagina facebook
footer +=`<a href="https://www.facebook.com/booksheartbrain/" target="_blank" rel="noopener noreferrer" title="Pagina Facebook"><i class="fa fa-facebook-square hvr-grow" aria-hidden="true" title="Facebook"></i></a> &nbsp;`;
//Account LivelloSegreto
footer +=`<a href="https://livellosegreto.it/@booksheartbrain" target="_blank" rel="noopener noreferrer" title="Account Mastodon"><i class="fa fa-mastodon hvr-grow" aria-hidden="true" title="Mastodon"></i></a> &nbsp;`;
//Account Lore LivelloSegreto (BookWyrm)
footer +=`<a href="https://lore.livellosegreto.it/user/booksheartbrain" target="_blank" rel="noopener noreferrer" title="Account Lore Bookwyrm"><img src="https://lore.livellosegreto.it/images/logos/lore_l_01_yellow_small.png" style="width: 27px; height: 27px;" class="fa hvr-grow" aria-hidden="true" title="Lore"></a> &nbsp;`;
//Podcast Funkwhale
footer +=`<a href="https://funkwhale.it/channels/paperbrains/" target="_blank" rel="noopener noreferrer" title="Funwhale Italia"><i class="fa fa-funkwhale hvr-grow" aria-hidden="true" title="Funkwhale Italia"></i></a> &nbsp;`;
//Podcast Spotify
footer +=`<a href="https://open.spotify.com/show/4q9EZgaGSfSPFKCZkBYKPI?si=3804fe0ed6474a06" target="_blank" rel="noopener noreferrer" title="Podcast su Spotify"><i class="fa fa-spotify hvr-grow" aria-hidden="true" title="Spotify"></i></a>`;
//Fine footer
footer += `</p></div>`;




document.getElementById("header").innerHTML = header;
document.getElementById("footer").innerHTML = footer;

// V bulma
document.addEventListener('DOMContentLoaded', () => {
    const $navbarBurgers = Array.prototype.slice.call(document.querySelectorAll('.navbar-burger'), 0);
    if ($navbarBurgers.length > 0) {
      $navbarBurgers.forEach(el => {
        el.addEventListener('click', () => {
          const target = el.dataset.target;
          const $target = document.getElementById(target);
          el.classList.toggle('is-active');
          $target.classList.toggle('is-active');
        });
      });
    }
});