var svolg = "Il gruppo di lettura si sta svolgendo";
var agg = "Questo mese il gruppo di lettura si è già svolto. Guarda il sito dal primo giorno del mese prossimo per sapere la data (solitamente l'ultimo venerdì del mese)"

function lastFridayOfMonth() {
	var today = new Date();
	var lastDay = new Date(today.getFullYear(), today.getMonth()+1, 0);
	if(lastDay.getDay() < 5) {
		lastDay.setDate(lastDay.getDate() - 7);
	}
	lastDay.setDate(lastDay.getDate() - (lastDay.getDay() -5));
	lastDay.setHours(16, 30, 0);
	return lastDay;
}

var countDownDate = lastFridayOfMonth().getTime();

var partecipa = document.URL.endsWith("partecipa.html");

// Aggiorna il countdown mostrato ogni secondo
var time = setInterval(function() {
	
	var now = new Date().getTime();
	var distance = countDownDate - now;

	var days = Math.floor(distance / (1000 * 60 * 60 * 24));
	var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
	var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));

	var testo = "Mancano " + ((days > 0)? days+" giorni, " : "") + ((hours > 0)? hours+" ore e " : "") + minutes +  " minuti al prossimo gruppo di lettura<br>La data potrebbe variare, se vuoi partecipare ed essere sicuro della data e dell'ora, contattaci in uno dei modi scritti ";

	// Mostra su index e partecipa due testi diversi
	if (partecipa) {
		document.getElementById("countdown").innerHTML = testo + "qui. Solitamente ci troviamo l'ultimo venerdì del mese alle 16:30";
	} else {
		document.getElementById("countdown").innerHTML = testo + "in <a href=\"/pages/partecipa.html\">Partecipa</a>";
	}
	
	if (distance < 0) {
		if(distance < -10800000) {
			document.getElementById("countdown").innerHTML = agg;
		} else {
			document.getElementById("countdown").innerHTML = svolg;
		}
		clearInterval(time);
	}
}, 1000);