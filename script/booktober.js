let anno;
const data = new Date();
const lista = [];

let now;
if (document.URL.includes("2021/10")) {
	anno = 2021;

	lista.push(new Libro(1, "La febbre zombie", "Kristina Ohlsson", "Insetti"));
	lista.push(new Libro(2, "Il giardino di mezzanotte", "Philippa Pearce", "Giardino"));
	lista.push(new Libro(3, "The Stone", "Guido Sgardoli", "Sasso"));
	lista.push(new Libro(4, "Orso, buco!", "Nicola Grossi", "Buco"));
	lista.push(new Libro(5, "Factory", "Tim Bruno", "Pecora"));
	lista.push(new Libro(6, "La bambina che amava Tom Gordon", "Stephen King", "Perdersi"));
	lista.push(new Libro(7, "La ragazza dei lupi", "Katherine Rundell", "Lupi"));
	lista.push(new Libro(8, "Il tesoro del labirinto incantato", "Elena Paccagnella e Nicoletta Bertelle", "Labirinto"));
	lista.push(new Libro(9, "Rughe", "Paco Roca", "Memoria"));
	lista.push(new Libro(10, "La grande fuga", "Ulf Stark", "Nonno"));
	lista.push(new Libro(11, "Tommy River sulla via del Nord", "Mino Milani", "Cowboy"));
	lista.push(new Libro(12, "Storie di cavalli", "Rolande Causse e Nane Vézinet", "Cavalli"));
	lista.push(new Libro(13, "Pashmina", "Nidhi Chanani", "Elefante"));
	lista.push(new Libro(14, "La straordinaria invenzione di Hugo Cabret", "Brian Selznick", "Luna"));
	lista.push(new Libro(15, "La nonna e le parole farfalla", "Francesca Mascheroni", "Nonna"));
	lista.push(new Libro(16, "Van Gogh. L'autobiografia mai scritta", "Marco Goldin", "Capelli rossi"));
	lista.push(new Libro(17, "Rory il dinosauro e il suo papà", "Liz Climo e anche \"Costruisci il tuo T.Rex e scopri il mondo dei dinosauri\" di Darren Naish", "T-rex"));
	lista.push(new Libro(18, "Giù le mani! Il pinguino Leo impara a difendersi dagli adulti", "Giuseppe Maiolo, Katia Schneider e Giulia Franchini", "Pinguino"));
	lista.push(new Libro(19, "L'oceano quando non ci sei", "Mark Lowery", "Al mare"));
	lista.push(new Libro(20, "Fuori dal guscio", "Jerry Spinelli", "Uovo"));
	lista.push(new Libro(21, "Timothy Top, libro uno: Verde cinghiale", "Gud", "Verde"));
	lista.push(new Libro(22, "Il leone e l'uccellino", "Marianne Dubuc", "Leone"));
	lista.push(new Libro(23, "Il soldatino di piombo", "Casa Editrice Piccoli", "Soldatino"));
	lista.push(new Libro(24, "Più veloce del vento", "Tommaso Percivale", "Vento"));
	lista.push(new Libro(25, "Gregor, la prima profezia", "Suzanne Collins", "Pipistrello"));
	lista.push(new Libro(26, "Pesce e gatto", "Joan Grant", "Nero"));
	lista.push(new Libro(27, "Bim! Bum! Bam!, strumenti musicali fatti in casa", "Maria Signorelli", "Tamburo"));
	lista.push(new Libro(28, "Palloncini: come modellarli", "Jon Tremaine", "Palloncino"));
	lista.push(new Libro(29, "Tutta colpa delle nuvole", "Mario Sala Gallini", "Nuvole"));
	lista.push(new Libro(30, "Non è colpa della pioggia", "Lyna Mullaly Hunt", "Pioggia"));
	lista.push(new Libro(31, "Pamela lo scoiattolo", "Olivier Seigneur e Frederic Bosc", "Scoiattolo"));
	
	if(anno === data.getFullYear() && data.getMonth() === 9) {
		now = data.getDate();
	} else {
		now = 31;
	}
	
} else if(document.URL.includes("2022/10")) {
	anno = 2022;

	lista.push(new Libro(1, "Sette minuti dopo la mezzanotte", "Patrick Ness", "Albero"));
	lista.push(new Libro(2, "Eppure sentire", "Cristina Bellemo", "Giraffe"));
	lista.push(new Libro(3, "La casa che mi porta via", "Sophie Anderson", "Strega"));
	lista.push(new Libro(4, "Le belve", "Manlio Castagna e Guido Sgardoli", "Paura"));
	lista.push(new Libro(5, "The skeleton tree", "Iain Lawrence", "Scheletri"));
	lista.push(new Libro(6, "I diari del limbo", "Manlio Castagna", "Sogni"));
	lista.push(new Libro(7, "Dimentica il mio nome", "Zerocalcare", "Volpe"));
	lista.push(new Libro(8, "Vorrei due ali", "Sandy Stark-McGinnis", "Rondini"));
	lista.push(new Libro(9, "La storia più importante", "Kenneth Oppel", "Macchia"));
	lista.push(new Libro(10, "Due mostri", "David Mckee", "David Mckee"));
	lista.push(new Libro(11, "Aspettando il vento", "Oskar Kroon", "Baci"));
	lista.push(new Libro(12, "La vacca nella selva", "Edy Lima", "Mucche"));
	lista.push(new Libro(13, "Le volpi del deserto", "Pierdomenico Baccalario", "Sabbia"));
	lista.push(new Libro(14, "Gli ottimisti muoiono prima", "Susin Nielsen", "Cappello"));
	lista.push(new Libro(15, "Attenti ai lupi. Le sette storie più spaventose dei fratelli Grimm", "Pierdomenico Baccalario e Davide Morosinotto", "Biancaneve"));
	lista.push(new Libro(16, "Ruby Bridges è entrata a scuola", "Elisa Puricelli Guerra", "Torta"));
	lista.push(new Libro(17, "Girls", "Jessica Schiefauer", "Semi"));
	lista.push(new Libro(18, "La più grande", "Davide Morosinotto", "Tigre"));
	lista.push(new Libro(19, "L'approdo", "Shaun Tan", "Silent Book"));
	lista.push(new Libro(20, "Perché io sono io e non sono te?", "Tomi Ungerer", "Tomi Ungerer"));
	lista.push(new Libro(21, "Olivia Kidney", "Ellen Potter", "Case"));
	lista.push(new Libro(22, "Funeral Party", "Guido Sgardoli", "Invenzioni"));
	lista.push(new Libro(23, "Gregor" , "Suzanne Collins", "Talpa"));
	lista.push(new Libro(24, "Il robot selvatico", "Peter Brown", "Pulcino"));
	lista.push(new Libro(25, "La verità secondo Mason Buttle", "Leslie Connor", "Autunno"));
	lista.push(new Libro(26, "Un polpo alla gola", "Zerocalcare", "Polpo"));
	lista.push(new Libro(27, "Il tiranno dei mondi", "Isaac Asimov", "Spazio"));
	lista.push(new Libro(28, "L'ultimo cacciatore", "Davide Morosinotto", "Preistoria"));
	lista.push(new Libro(29, "L'oceano quando non ci sei", "Mark Lowery", "Viaggio"));
	lista.push(new Libro(30, "Maionese, ketchup e latte di soia", "Gaia Guasti", "Latte"));
	lista.push(new Libro(31, "Babbo Natale", "Raymond Briggs", "Raymond Briggs"));


	if(anno === data.getFullYear() && data.getMonth() === 9) {
		now = new data.getDate();
	} else {
		now = 31;
	}
}

StampaLista(lista);
InserisciImmagini();

function Libro(giorno, titolo, autore, tema){
	this.giorno = giorno;
	this.titolo = titolo;
	this.autore = autore;
	this.tema = tema;
   
	this.getGiorno = function(){
		return this.giorno;
	};
	this.getTitolo = function(){
		return this.titolo;
	};
	this.getAutore = function(){
		return this.autore;
	};
	this.getTema = function(){
		return this.tema;
	}
}

function StampaLista(lista){
	const testi = document.getElementById("testi");
	const divtesti = document.createElement("div");

	for(let i=0; i<now; i++){
		const div = divtesti.appendChild(document.createElement("div"));
		div.setAttribute("id", (i+1) + "-o");
		div.setAttribute("class", "section box block hvr-glow textboxfull");

		const titolo = div.appendChild(document.createElement("h2"));
		titolo.setAttribute("class", "is-size-2");
		titolo.appendChild(document.createTextNode(lista[i].getTema()));

		const testo = div.appendChild(document.createElement("p"));
		testo.appendChild(document.createTextNode("Il libro che ha come tema \"" + lista[i].getTema() + "\" per il giorno " + lista[i].getGiorno() + " ottobre " + anno + " è \"" + lista[i].getTitolo() + "\" di " + lista[i].getAutore()));
	}

	testi.appendChild(divtesti);
}

function InserisciImmagini(){
	const immagini = document.getElementById("immagini");
	const divimmagini = document.createElement("div");
	divimmagini.setAttribute("class", "py-3 is-flex is-flex-direction-row is-flex-wrap-wrap is-justify-content-space-around is-align-content-space-around");

	for(let i=0; i<now; i++){
		const div = divimmagini.appendChild(document.createElement("div"));
		div.setAttribute("class", "image-container");

		const img = div.appendChild(document.createElement("img"));
		img.setAttribute("alt", "");
		img.setAttribute("src", "/assets/booktober/" + anno + "/" + (i+1) + "ott.webp");

		const link = div.appendChild(document.createElement("a"));
		link.setAttribute("href", "#" + (i+1) + "-o");

		const caption = link.appendChild(document.createElement("div"));
		caption.setAttribute("class", "image-caption caption-1");

		caption.appendChild(document.createElement("br")); //tag br

		const data = caption.appendChild(document.createElement("p"));
		data.appendChild(document.createTextNode(lista[i].getGiorno() + " Ottobre"));
		const tema = caption.appendChild(document.createElement("p"));
		tema.appendChild(document.createTextNode(lista[i].getTema()));

		caption.appendChild(document.createElement("br")); //tag br

		const titolo = caption.appendChild(document.createElement("p"));
		titolo.appendChild(document.createTextNode(lista[i].getTitolo()));
	}

	immagini.appendChild(divimmagini);
}
